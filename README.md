## Run Locally
- clone this project
- run `yarn install`
- run postgresql database in docker
    ```bash
        docker run --rm \
        --name todoapp-db \
        -e POSTGRES_DB=todoapp_db  \
        -e POSTGRES_USER=husfuu \
        -e POSTGRES_PASSWORD=mysecretpassword \
        -e PGDATA=/var/lib/postgresql/data/pgdata \
        -v "$PWD/todoapp-db-data:/var/lib/postgresql/data" \
        -p 5432:5432 \
        postgres:13
    ```
- run `yarn dev`
