import config from 'config';
import express from 'express';
import swaggerDocs from './helper/swagger';
import taskRouter from './routers/task.routers';
import userRouter from './routers/user.routers';

const port = config.get("port") as number;
const host = config.get("host") as string;

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }))
app.use(taskRouter);
app.use(userRouter);

app.listen(port, () => {
    console.log("Application is running")

    swaggerDocs(app, port)
})