import { Request, Response } from "express";
import { InternalServerError } from "../helper/error";
import taskService from "../service/task.service";

interface TaskControllerInterface {
    findAll: (request: Request, response: Response) => void
    findById: (request: Request, response: Response) => void
    create: (request: Request, response: Response) => void
    update: (request: Request, response: Response) => void
    updateStatus: (request: Request, response: Response) => void
    delete: (request: Request, response: Response) => void
}

class TaskController implements TaskControllerInterface {
    findAll = async (request: Request, response: Response) => {
        try {
            const result = await taskService.findAll();
            response.json(result);
        } catch (error) {
            const internaleError = new InternalServerError("internal error");
            return response.status(internaleError.statusCode).json({
                status: internaleError.statusCode,
                message: internaleError.getErrorMessage
            })
        }
    }

    findById = async (request: Request, response: Response) => {
        try {
            const taskId = request.params.id;
            const result = await taskService.findById(taskId);
            response.json(result);
        } catch (error) {
            const internaleError = new InternalServerError("internal error");
            return response.status(internaleError.statusCode).json({
                status: internaleError.statusCode,
                message: internaleError.getErrorMessage
            })
        }
    }

    create = async (request: Request, response: Response) => {
        try {
            const result = await taskService.create(request.body);
            response.json(result);
        } catch (error) {
            const internaleError = new InternalServerError("internal error");
            return response.status(internaleError.statusCode).json({
                status: internaleError.statusCode,
                message: internaleError.getErrorMessage
            })
        }
    }

    update = async (request: Request, response: Response) => {
        try {
            const taskId = request.params.id;
            const result = await taskService.update(request.body, taskId);
            response.json(result);
        } catch (error) {
            const internaleError = new InternalServerError("internal error");
            return response.status(internaleError.statusCode).json({
                status: internaleError.statusCode,
                message: internaleError.getErrorMessage
            })
        }
    }

    updateStatus = async (request: Request, response: Response) => {
        try {
            const taskId = request.params.id;
            const result = await taskService.updateStatus(request.body, taskId);
            response.json(result);
        } catch (error) {
            const internaleError = new InternalServerError("internal error");
            return response.status(internaleError.statusCode).json({
                status: internaleError.statusCode,
                message: internaleError.getErrorMessage
            })
        }
    }

    delete = async (request: Request, response: Response) => {
        try {
            const taskId = request.params.id;
            const result = await taskService.delete(taskId);
            response.json(result);
        } catch (error) {
            const internaleError = new InternalServerError("internal error");
            return response.status(internaleError.statusCode).json({
                status: internaleError.statusCode,
                message: internaleError.getErrorMessage
            })
        }
    }
}

export default new TaskController();