import { Request, Response } from "express";
import { AlreadyExistError, BadRequestError, ForbidenError, InternalServerError, NotFoundError } from "../helper/error";
import userService from "../service/user.service";

interface UserControllerInterface {
    signUp: (request: Request, response: Response) => void
    signIn: (request: Request, response: Response) => void
}

class UserController implements UserControllerInterface {

    signUp = async (request: Request, response: Response) => {
        try {
            if (!request.body.username || !request.body.password) {
                throw new BadRequestError("fill the required field of request body")
            }
            const result = await userService.signUp(request.body);

            response.json(result)
        } catch (error) {
            if (error instanceof AlreadyExistError) {
                return response.status(error.statusCode).json({
                    status: error.statusCode,
                    message: error.getErrorMessage(),
                });
            };

            const internalError = new InternalServerError("internal error");
            return response.status(internalError.statusCode).json({
                status: internalError.statusCode,
                message: internalError.getErrorMessage(),
            });
        }
    }


    signIn = async (request: Request, response: Response) => {
        try {
            if (!request.body.username || !request.body.password) {
                throw new BadRequestError("fill username or password")
            };

            const result = await userService.signIn(request.body);
            response.json(result);
        } catch (error) {
            if (error instanceof NotFoundError) {
                return response.status(error.statusCode).json({
                    status: error.statusCode,
                    message: error.getErrorMessage(),
                });
            };

            if (error instanceof ForbidenError) {
                return response.status(error.statusCode).json({
                    status: error.statusCode,
                    message: error.getErrorMessage(),
                });
            };

            const internalError = new InternalServerError("internal error");
            return response.status(internalError.statusCode).json({
                status: internalError.statusCode,
                message: internalError.getErrorMessage(),
            });
        }
    }
}

export default new UserController();