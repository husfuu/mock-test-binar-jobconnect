/**
 * @openapi
 * components:
 *  schemas:
 *    CreateTaskInput:
 *      type: object
 *      required:
 *        - title
 *        - description
 *      properties:
 *        title:
 *          type: string
 *          default: First task
 *        description:
 *          type: string
 *          default: First task's description
 *    CreateTaskResponse:
 *      type: object
 *      properties:
 *        message:
 *          type: string
 *          default: success to created task
 *    UpdateTaskResponse:
 *      type: object
 *      properties:
 *        message:
 *          type: string
 *          default: success to update task
 *    DeleteTaskResponse:
 *      type: object
 *      properties:
 *        message:
 *          type: string
 *          default: success to delete task
 *    GetTaskByIdResponse:
 *      type: object
 *      properties:
 *        title:
 *          type: string
 *          default: ini judul
 *        description:
 *          type: string
 *          default: ini description
 *        isFinished:
 *          type: boolean
 *    GetAllTasksResponse:
 *      type: array
 *      items:
 *        $ref: '#/components/schemas/GetTaskByIdResponse'      
 */

export type RequestTaskType = {
    title: string
    description: string
    isFinished: boolean
}

export type ResponseTaskType = {
    message: string
}

export type ResponseTaskDataType = {
    title: string
    description: string
    isFinished: boolean
}

export type ResponseTaskFindById = {
    message: string
    data: ResponseTaskDataType
}

export type ResponseTaskFindAllType = {
    message: string
    data: ResponseTaskDataType[]
}