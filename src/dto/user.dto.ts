/**
 * @openapi
 * components:
 *  schemas:
 *    CreateUserInput:
 *      type: object
 *      required:
 *        - username
 *        - password
 *      properties:
 *        username:
 *          type: string
 *          default: Jane Doe
 *        password:
 *          type: string
 *          default: stringPassword123
 *    CreateUserResponse:
 *      type: object
 *      properties:
 *        message:
 *          type: string
 *          default: success to created user
 *    LoginUserResponse:
 *      type: object
 *      properties:
 *        message:
 *          type: string
 *          default: success to login user
 *        token:
 *          type: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJJZCI6MSwidXNlcm5hbWUiOiJodXNmdXUifSwiaWF0IjoxNjcwMDgxMjE0fQ.WdMes5VoOYhc69Q-WjV-5TbwBzs_T5gzSa5akqmOaUo
 */
export type RequestUserType = {
    username: string
    password: string,
}

export type ResponseUserType = {
    message?: string
    token?: string
}