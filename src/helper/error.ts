export class NotFoundError extends Error {
    statusCode = 404;

    constructor(message: string) {
        super(message);

        Object.setPrototypeOf(this, NotFoundError.prototype);
    }

    getErrorMessage() {
        return "[not found error]: " + this.message;
    }
}

export class AlreadyExistError extends Error {
    statusCode = 403;

    constructor(message: string) {
        super(message);

        Object.setPrototypeOf(this, AlreadyExistError.prototype);
    }

    getErrorMessage() {
        return "[already exists error]: " + this.message;
    }
}

export class BadRequestError extends Error {
    statusCode = 400;

    constructor(message: string) {
        super(message);

        Object.setPrototypeOf(this, BadRequestError.prototype);
    }

    getErrorMessage() {
        return "[bad request error]: " + this.message;
    }
}

export class Unauthorized extends Error {
    statusCode = 401;

    constructor(message: string) {
        super(message);

        Object.setPrototypeOf(this, Unauthorized.prototype);
    }

    getErrorMessage() {
        return "[unauthorized error]: " + this.message;
    }
}

export class ForbidenError extends Error {
    statusCode = 403;

    constructor(message: string) {
        super(message);

        Object.setPrototypeOf(this, ForbidenError.prototype);
    }

    getErrorMessage() {
        return "[forbidden error]: " + this.message;
    }
}


export class InternalServerError extends Error {
    statusCode = 500;

    constructor(message: string) {
        super(message);

        Object.setPrototypeOf(this, InternalServerError.prototype);
    }

    getErrorMessage() {
        return "[internal server error]: " + this.message;
    }
}