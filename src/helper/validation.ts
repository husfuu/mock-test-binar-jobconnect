import db from "../models"
import { AlreadyExistError } from "./error";

export async function isUserExists(username: string) {
    const user = await db.Users.findOne({
        where: {
            username: username
        }
    });

    return user;
}