import { Request, Response, NextFunction } from 'express';
import AuthService from '../service/auth.service';

export function authorizationMiddleware(
    req: Partial<Request>,
    res: Partial<Response>,
    next: NextFunction
): void {
    const token = req.headers?.authorization
        ? req.headers.authorization.replace("Bearer ", "")
        : null;

    try {
        const claims = AuthService.decodeToken(token as string);
        // console.log(claims.sub)
        req.context = claims.user
        // console.log(req.context)

        next();
    } catch (error) {
        if (error instanceof Error) {
            res.status?.(401).send({ code: 401, error: error.message });
        } else {
            res.status?.(401).send({ code: 401, error: 'Unknown auth error' });
        }
    }


}