import { Router } from "express";
import taskController from "../controllers/task.controller";

const router = Router();
/**
 * @openapi
 * '/api/v1/tasks':
 *  get:
 *     tags:
 *     - Tasks
 *     summary: Get all tasks
 *     responses:
 *       200:
 *         description: successfully get all tasks
 *         content: 
 *            application/json:
 *              schema:
 *                type: array
 *                items:
 *                  $ref: '#/components/schemas/GetTaskByIdResponse'
 *       500:
 *         description: internal error
 */
router.get("/api/v1/tasks", taskController.findAll);

/**
 * @openapi
 * '/api/v1/tasks/:id':
 *  get:
 *     tags:
 *     - Tasks
 *     summary: Get task by id
 *     responses:
 *       200:
 *         description: successfully get tasks by id
 *         content: 
 *            application/json:
 *              schema:
 *                type: object
 *                $ref: '#/components/schemas/GetTaskByIdResponse'
 *       500:
 *         description: internal error
 */
router.get("/api/v1/tasks/:id", taskController.findById);

/**
 * @openapi
 * '/api/v1/tasks/':
 *  post:
 *     tags:
 *     - Tasks
 *     summary: Create a new task
 *     requestBody:
 *      required: true
 *      content:
 *        application/json:
 *           schema:
 *              $ref: '#/components/schemas/CreateTaskInput'
 *     responses:
 *       200:
 *         description: successfully created task
 *         content: 
 *            application/json:
 *              schema:
 *                type: object
 *                $ref: '#/components/schemas/CreateTaskResponse'
 *       500:
 *         description: internal error
 */
router.post("/api/v1/tasks", taskController.create);


/**
 * @openapi
 * '/api/v1/tasks/:id':
 *  put:
 *     tags:
 *     - Tasks
 *     summary: Update task by id
 *     requestBody:
 *      required: true
 *      content:
 *        application/json:
 *           schema:
 *              $ref: '#/components/schemas/CreateTaskInput'
 *     responses:
 *       200:
 *         description: successfully updated task
 *         content: 
 *            application/json:
 *              schema:
 *                type: object
 *                $ref: '#/components/schemas/UpdateTaskResponse'
 *       500:
 *         description: internal error
 */
router.put("/api/v1/tasks/:id", taskController.update);

/**
 * @openapi
 * '/api/v1/tasks/status/:id':
 *  put:
 *     tags:
 *     - Tasks
 *     summary: Update task status by id
 *     requestBody:
 *      required: true
 *      content:
 *        application/json:
 *           schema:
 *             type: object
 *             properties:
 *               isFinished: boolean
 *               default: true
 *     responses:
 *       200:
 *         description: successfully updated task status
 *         content: 
 *            application/json:
 *              schema:
 *                type: object
 *                $ref: '#/components/schemas/UpdateTaskResponse'
 *       500:
 *         description: internal error
 */
router.put("/api/v1/tasks/status/:id", taskController.updateStatus);

/**
 * @openapi
 * '/api/v1/tasks/:id':
 *  delete:
 *     tags:
 *     - Tasks
 *     summary: Delete task by id
 *     responses:
 *       200:
 *         description: successfully deleted task
 *         content: 
 *            application/json:
 *              schema:
 *                type: object
 *                $ref: '#/components/schemas/DeleteTaskResponse'
 *       500:
 *         description: internal error
 */
router.delete("/api/v1/tasks/:id", taskController.delete);

export default router;