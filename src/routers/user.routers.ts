import { Router } from "express";
import userController from "../controllers/user.controller";

const router = Router();
/**
 * @openapi
 * '/api/v1/signup':
 *  post:
 *     tags:
 *     - Users
 *     summary: Create a new user
 *     requestBody:
 *      required: true
 *      content:
 *        application/json:
 *           schema:
 *              $ref: '#/components/schemas/CreateUserInput'  
 *     responses:
 *       200:
 *         description: successfully created user
 *         content: 
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/CreateUserResponse'
 *       403:
 *         description: user is already exists
 *       500:
 *         description: internal error
 */

router.post("/api/v1/signup", userController.signUp)

/**
 * @openapi
 * '/api/v1/signin':
 *  post:
 *     tags:
 *     - Users
 *     summary: Sign in user
 *     requestBody:
 *      required: true
 *      content:
 *        application/json:
 *           schema:
 *              $ref: '#/components/schemas/CreateUserInput'  
 *     responses:
 *       200:
 *         description: successfully login user
 *         content:
 *            application/json:
 *              schema:
 *               $ref: '#/components/schemas/LoginUserResponse'
 *       500:
 *         description: internal server
  */
router.post("/api/v1/signin", userController.signIn)

export default router;