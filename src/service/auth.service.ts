import jwt from "jsonwebtoken";

const { JWT_KEY } = process.env;

export interface JwtToken {
    user: {
        userId: string,
        username: string
    }
}

export default class AuthService {
    public static decodeToken(token: string): JwtToken {
        return jwt.verify(token, JWT_KEY as string) as JwtToken;
    }
}