import { RequestTaskType, ResponseTaskDataType, ResponseTaskFindAllType, ResponseTaskFindById, ResponseTaskType } from "../dto/task.dto"
import { InternalServerError } from "../helper/error"
import db from "../models"

interface TaskServiceInterface {
    findAll: () => Promise<ResponseTaskFindAllType>
    findById: (taskId: string) => Promise<ResponseTaskFindById>
    create: (input: RequestTaskType) => Promise<ResponseTaskType>
    update: (input: RequestTaskType, taskId: string) => Promise<ResponseTaskType>
    updateStatus: (finished: boolean, taskId: string) => Promise<ResponseTaskType>
    delete: (taskId: string) => Promise<ResponseTaskType>
}

class TaskService implements TaskServiceInterface {
    findAll = async (): Promise<ResponseTaskFindAllType> => {
        try {
            const tasks: ResponseTaskDataType[] = await db.Tasks.findAll();

            return {
                message: "success to get all tasks",
                data: tasks
            }
        } catch (error) {
            throw new InternalServerError("internal server error");
        }
    }

    findById = async (taskId: string): Promise<ResponseTaskFindById> => {
        try {
            const task: ResponseTaskDataType = await db.Tasks.findByPk(taskId);

            return {
                message: `success to get task with id = ${taskId}`,
                data: task
            }
        } catch (error) {
            throw new InternalServerError("internal server error");
        }
    }

    create = async (input: RequestTaskType): Promise<ResponseTaskType> => {
        try {
            await db.Tasks.create({
                title: input.title,
                description: input.description,
                isFinished: input.isFinished ? input.isFinished : false
            });

            return {
                message: "success to create task"
            }
        } catch (error) {
            throw new InternalServerError("internal server error");
        }
    }

    update = async (input: RequestTaskType, taskId: string): Promise<ResponseTaskType> => {
        try {
            await db.Tasks.update(
                {
                    title: input.title,
                    description: input.description,
                    isFinished: input.isFinished
                },
                { where: { id: taskId } }
            );

            return {
                message: `success to update task with id = ${taskId}`
            }
        } catch (error) {
            throw new InternalServerError("internal server error");
        }
    }

    updateStatus = async (finished: boolean, taskId: string): Promise<ResponseTaskType> => {
        try {
            await db.Tasks.update(
                {
                    isFinished: finished
                },
                { where: { id: taskId } }
            )

            console.log(finished);
            return {
                message: `success to update status task with id = ${taskId} to ${finished}`
            }

        } catch (error) {
            throw new InternalServerError("internal server error");
        }
    }

    delete = async (taskId: string): Promise<ResponseTaskType> => {
        try {
            await db.Tasks.destroy({
                where: { id: taskId }
            })

            return {
                message: `success to delete task with id = ${taskId}`
            }
        } catch (error) {
            throw new InternalServerError("internal server error");
        }
    }
}

export default new TaskService();