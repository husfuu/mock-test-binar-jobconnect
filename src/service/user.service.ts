import { RequestUserType, ResponseUserType } from "../dto/user.dto";
import { isUserExists } from "../helper/validation"
import bcrypt from "bcrypt";
import db from "../models";
import { AlreadyExistError, ForbidenError, InternalServerError, NotFoundError } from "../helper/error";
import jwt from "jsonwebtoken";

const { JWT_KEY } = process.env;

interface UserServiceInterface {
    signUp: (input: RequestUserType) => Promise<ResponseUserType>
    signIn: (input: RequestUserType) => Promise<ResponseUserType>
}

class UserService implements UserServiceInterface {
    signUp = async (input: RequestUserType): Promise<ResponseUserType> => {
        try {
            const user = await isUserExists(input.username);
            if (user) {
                throw new AlreadyExistError("user is already exists")
            }

            // has password process
            const salt = bcrypt.genSaltSync(10);
            const hashedPassword = bcrypt.hashSync(input.password, salt);

            await db.Users.create({
                username: input.username,
                password: hashedPassword
            });

            return {
                message: "successfully created user"
            }
        } catch (error) {
            throw new InternalServerError("internal server error");
        }
    }
    signIn = async (input: RequestUserType): Promise<ResponseUserType> => {
        try {
            const user = await isUserExists(input.username);

            if (!user) {
                throw new NotFoundError("user is not exists");
            }

            const checkPassword: boolean = bcrypt.compareSync(input.password, user.password);

            if (checkPassword) {
                const token: string = jwt.sign({
                    user: {
                        userId: user.id,
                        username: user.username,
                    },
                }, JWT_KEY as string)

                return {
                    message: "login successfully",
                    token: token
                }
            } else {
                throw new ForbidenError("password is false");
            }
        } catch (error) {
            if (error instanceof ForbidenError) {
                throw new ForbidenError("password is false");
            }
            throw new InternalServerError("internal server error");
        }
    }
}

export default new UserService();